/*
 * Copyright Andret (c) 2018. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.parkour.sample;

public enum BalanceInteraction {
	ADD,
	SUB,
	SET
}
