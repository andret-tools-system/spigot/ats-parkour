The plugin "<a href="https://gitlab.com/andret-tools-system/spigot/ats-parkour/" target="_blank">atsParkour</a>"
by <a href="https://gitlab.com/andret2344" target="_blank">Andret2344</a> belongs
to <a href="https://gitlab.com/andret-tools-system"  target="_blank">Andret Tools System</a> group and is licensed
under <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank">CC BY-SA v4.0</a> license.
