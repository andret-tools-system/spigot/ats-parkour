/*
 * Copyright Andret (c) 2018. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.parkour.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {
	public static final String AMP = "%AMP%";
	public static final String AMPLIFIER = "%AMPLIFIER%";
	public static final String ARGUMENT = "%ARGUMENT%";
	public static final String BALANCE = "%BALANCE%";
	public static final String COORD_PITCH = "%COORD_PITCH%";
	public static final String COORD_X = "%COORD_X%";
	public static final String COORD_Y = "%COORD_Y%";
	public static final String COORD_YAW = "%COORD_YAW%";
	public static final String COORD_Z = "%COORD_Z%";
	public static final String COUNT = "%COUNT%";
	public static final String DESCRIPTION = "%DESCRIPTION%";
	public static final String DISPLAY_NAME = "%DISPLAY_NAME%";
	public static final String EFFECT = "%EFFECT%";
	public static final String ENABLED = "%ENABLED%";
	public static final String FEE = "%FEE%";
	public static final String INDEX = "%INDEX%";
	public static final String MEDAL = "%MEDAL%";
	public static final String MEDALS = "%MEDALS%";
	public static final String NAME = "%NAME%";
	public static final String NEW_NAME = "%NEW_NAME%";
	public static final String NICK = "%NICK%";
	public static final String NUMBER = "%NUMBER%";
	public static final String OLD_NAME = "%OLD_NAME%";
	public static final String OPTION = "%OPTION%";
	public static final String PAGE = "%PAGE%";
	public static final String PAGES = "%PAGES%";
	public static final String PERSONAL_TIME = "%PERSONAL_TIME%";
	public static final String PLACEHOLDER_NO_RECORD = "========";
	public static final String PLAYER = "%PLAYER%";
	public static final String REWARD = "%REWARD%";
	public static final String RUNNING = "%RUNNING%";
	public static final String SECONDS = "%SECONDS%";
	public static final String TIME = "%TIME%";
	public static final String VALUE = "%VALUE%";
	public static final String VIP_ONLY = "%VIP_ONLY%";

	public static final String KEY_MEDAL = "medal";
	public static final String PARKOUR = "parkour";
}
