/*
 * Copyright Andret (c) 2018. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.parkour.entity;

public enum SimpleLever {
	ON,
	OFF
}
